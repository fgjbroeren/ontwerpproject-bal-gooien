# Wat moet je inleveren?

Aan het einde van dit project lever je drie dingen in:

- Jouw knikkerwerpapparaat
  - Deze bouw je op school in de les en lever je in na de laatste ontwerples
- Een filmpje van het apparaat tijdens de test
  - Deze lever je na het testmoment in op ELO
- [Het eindverslag](verslag)
  - Deze lever je digitaal in op ELO
