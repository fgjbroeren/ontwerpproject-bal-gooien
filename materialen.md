(materialen)=
# Beschikbare Materialen

Voor dit project mag je alleen gebruikelijke huis-tuin-en-keuken materialen gebruiken. Op school hebben wij in ieder geval de volgende materialen beschikbaar:

- Karton
- Papier
- Elastiek
- Balonnen
- Satéprikkers
- Plakband
- Lijm

Als je andere materialen nodig hebt kan je deze van huis meenemen. Vraag alleen wel eerst aan een docent of deze toegestaan zijn.