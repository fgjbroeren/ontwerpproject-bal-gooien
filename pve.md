(pve)=
# Programma van Eisen

Het programma van eisen is een lijst met de verschillende dingen waar jouw ontwerp aan moet voldoen. Deze zijn in drie categorieën gesplitst: Randvoorwaarden, Functionele Eisen en Prestatiecriteria. 

Aan de randvoorwaarden en de functionele eisen _moet_ je ontwerp voldoen. Onder randvoorwaarden vind je hoeveel ruimte je hebt om in te ontwerpen, wat voor materialen je mag gebruiken enzovoorts. De functionele eisen is alles wat je ontwerp moet kunnen doen.

Wanneer aan alle randvoorwaarden en functionele eisen is voldaan kijken je naar de prestatiecriteria. Deze bepalen hoe goed het ontwerp is. Deze helpen je in het ontwerpproces met het kiezen tussen verschillende opties.

## Randvoorwaarden

- Het apparaat is gemaakt van [huis-tuin-en-keukenmaterialen](materialen)
- Het apparaat past binnen de ontwerpruimte van 25 cm hoog en 40 cm in de werprichting
- De knikkers die geworpen worden hebben een diameter van 16,5 mm en wegen ongeveer 6 gram

## Functionele Eisen

- Het apparaat werpt een knikker over een obstakel en in een bakje
- Het apparaat komt in beweging na een eenvoudige handeling die geen energie toevoegt aan het systeem

## Prestatiecriteria

_Deze lijst geeft een begin, maar is mogelijk niet compleet of uitgebreid genoeg. Je kan hier je eigen criteria aan toevoegen of je kan de criteria die gegeven zijn verder specificeren. Denk ook voor elk van deze criteria na hoe je zou kunnen testen of je ontwerp eraan voldoet._

- Het apparaat is eenvoudig te activeren
- Het apparaat is eenvoudig terug te zetten zodat een nieuwe knikker gegooid kan worden
- De boog die de geworpen knikker maakt is telkens hetzelfde