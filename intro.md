# Inleiding

In dit project ontwerp je een apparaat dat een knikker over een obstakel en in een bakje werpt. Hierbij is het belangrijk dat het apparaat dit precies en herhaalbaar doet. Dit doe je aan de hand van [](cyclus).

## Inspiratie

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ssZ_8cqfBlE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Veel bedrijven gebruiken robots om voorwerpen te verzamelen en in te pakken. In het filmpje hierboven zie je een voorbeeld daarvan. Je ziet dat er een ingewikkeld systeem van meerdere robots nodig is om boodschappen te sorteren. 

Gelukkig worden de meeste robots toegepast in meer gestructureerde omgevingen. Je weet dan precies welke vorm de producten die opgepakt moeten worden hebben. Vaak moeten deze producten ook telkens over dezelfde afstand verplaatst worden. In deze opdracht is dat ook het geval.

## De opdracht

Voor dit project is de opdrachtgever een knikkerfabrikant. Deze fabrikant zoekt naar een manier om zijn knikkers snel en gecontroleerd in bakken te kunnen verzamelen. Jij bent gevraagd om mee te helpen door een onderdeel van deze robot te ontwerpen, namelijk het gooimechanisme. Dit onderdeel moet ervoor zorgen dat de bal over een obstakel in de bak gegooid wordt. Een schets van de situatie vind je hieronder.

![](Schets.png)

Omdat het gooimechanisme onderdeel wordt van een grotere machine zijn er wel enkele beperkingen:

- Het mechanisme mag niet buiten de aangegeven ontwerpruimte komen
- Het mechanisme moet met een enkele handeling, die geen energie aan het apparaat toevoegt, te activeren zijn

Daarnaast worden er enkele beperkingen toegevoegd doordat wij het in de klas ontwikkelen:

- Het mechanisme moet bestaan uit huis-tuin-en-keuken materialen

Verdere details zijn te vinden op de pagina [](opdracht).

# PDF

Een PDF van deze webpagina is [hier beschikbaar](https://gitlab.com/fgjbroeren/ontwerpproject-bal-gooien/-/jobs/artifacts/main/raw/book.pdf?job=latex)

# Slides

Die zijn hier te vinden: [https://homepage.tudelft.nl/9h63a/Natuurkunde/Havo5/Ontwerpen/Slides.html](https://homepage.tudelft.nl/9h63a/Natuurkunde/Havo5/Ontwerpen/Slides.html)