# Conceptueel ontwerp

De eerste stap die we doorlopen is het conceptueel ontwerp. Hierin bedenken we welke deelfuncties het uiteindelijke apparaat uit moet voeren en bedenken we voor elke deelfunctie hoe we dat zouden kunnen realiseren.

## Deelfuncties

Eerst bedenken we deelfuncties die ons apparaat moet kunnen uitvoeren. In het [](pve) staan de functionele eisen al. Dit zijn de functies die het gehele apparaat uit moet kunnen voeren. Vaak zijn er echter ook nog verschillende deelfuncties die ervoor zorgen dat het apparaat goed werkt. Door deelfuncties te formuleren splits je het probleem eigenlijk op. Vaak is het moeilijk om te bedenken hoe je het hele probleem op kan lossen, of kom je eigenlijk maar op één idee. Voor deelfuncties is het vaak makkelijker om verschillende oplossingsrichtingen te bedenken. 

Om je alvast op weg te helpen kan je de volgende deelfuncties gebruiken, voel je vrij om deze lijst nog aan te vullen.

- Energie opslaan
- Mechanisme activeren/in beweging zetten
- Knikker werpen
- Knikker vasthouden

## Ideeëntabel

Als je verschillende deelfuncties voor je ontwerp hebt bedacht kan je voor elk van deze deelfuncties bedenken hoe je deze zou kunnen realiseren. Laat je fantasie hier de vrije loop en stel jezelf telkens de vraag "Hoe kun je ... ?".

Deze mogelijke manieren om de deelfuncties te realiseren kan je in een ideeëntabel zetten. Deze tabel bestaat uit even veel rijen als je deelfuncties hebt. Aan het begin van elke rij zet je een korte omschrijving van de deelfunctie. Daarachter zet je de verschillende manieren om deze functies te realiseren. Het werkt vaak het beste door dit met een kleine tekening en/of een korte omschrijving te doen. Maak dit niet te uitgebreid, de bedoeling is om in één oogopslag te kunnen herkennen wat de bedoelde oplossing is. Probeer ten minste drie verschillende ideeën per deelfunctie te verzinnen.

Een voorbeeld van een ideeëntabel staat hieronder. Het ontwerpprobleem was in dit geval een apparaat die een appel op kon pakken, verplaatsen en daarna weer neer kon zetten.

![](https://project.3me.tudelft.nl/2017/wb31/img/kaart/morfologischkaart.jpeg)