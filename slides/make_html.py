import glob
import os, time
from datetime import datetime
import subprocess
from webdav3.client import Client
from webdav3.exceptions import ResponseErrorCode
import keyring

passwd = keyring.get_password('TUD_NetID', 'fbroeren')

options = {
       'webdav_hostname':   'https://webdata.tudelft.nl',
       'webdav_login':      'fbroeren',
       'webdav_password':   passwd,
       'root': '/staff-homes/b/fbroeren/www'
}

cmd = ['pandoc',
       '-s',
       '-t', 'revealjs',
       '-V', 'revealjs-url=https://homepage.tudelft.nl/9h63a/Natuurkunde/reveal.js',
       '--standalone',
       '--slide-level', '2',
       '--mathjax']

klas = 'Havo5'
hfdst = 'Ontwerpen'

for f in glob.glob('*.md'):
    outfile = 'html/' + f[:-3]+'.html'
    print(f, '-->', outfile)
    subprocess.run(cmd + [f, '-o', outfile], check=True)

try:
       client = Client(options)
       print(client.list())
       connected = True 
except ResponseErrorCode:
       print('Verbinden met de server is niet gelukt, bestanden worden niet ge-upload')
       connected = False

if connected:
       for f in glob.glob('html/*.html'):
              if client.check('Natuurkunde/'+klas+'/'+hfdst+'/'+f[5:]):
                     info_remote = client.info('Natuurkunde/'+klas+'/'+hfdst+'/'+f[5:])
                     info_local = {'created': os.path.getctime(f),
                                   'modified': os.path.getmtime(f)}
       
                     modified_remote = datetime.strptime(info_remote['modified'], '%a, %d %b %Y %H:%M:%S %Z')
                     modified_local = datetime.utcfromtimestamp(info_local['modified'])
                     print(f'''
           Local                    | Remote
Created    {datetime.utcfromtimestamp(info_local['created'])} | {info_remote['created']}
Modified   {modified_local} | {modified_remote}
                             '''
                     )
                     print(modified_remote < modified_local)
                     if modified_remote < modified_local:
                            client.upload_sync(remote_path='Natuurkunde/'+klas+'/'+hfdst+'/'+f[5:], local_path=f)
              else:
                     client.upload_sync(remote_path='Natuurkunde/'+klas+'/'+hfdst+'/'+f[5:], local_path=f)
       for f in glob.glob('img/*'):
              if client.check('Natuurkunde/'+klas+'/'+hfdst+'/'+f):
                     pass
              else:
                     print('uploading',f)
                     client.upload_sync(remote_path='Natuurkunde/'+klas+'/'+hfdst+'/'+f, 
                                        local_path=f)
       
       
       l = client.list('Natuurkunde/'+klas+'/'+hfdst+'/')
       print(l)
       