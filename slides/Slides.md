---
title: "PO Ontwerpen"
subtitle: "HAVO 5"
date: "13 - 21 oktober 2022"
author: "F.G.J. Broeren"
theme: white
slidenumber: true
center: true
width: 1920
---

# Waarom ontwerpen?

## Alles om ons heen is ontworpen

::::::{.columns}
:::{.column width=50%}
<img src=https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.contemporist.com%2Fwp-content%2Fuploads%2F2016%2F11%2Fmodern-rocking-chair-wood-251116-1211-12-800x761.jpg&f=1&nofb=1&ipt=87a42a7dd782111800ebd94f6691222df694acf5e98968437bbedc2587c0d6e7&ipo=images height=350px>

<img src=https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.medichain.nl%2Fmedia%2Fcatalog%2Fproduct%2Fcache%2F1%2Fimage%2F1800x%2F040ec09b1e35df139433887a97daa66f%2F2%2F5%2F25131600_2.jpg&f=1&nofb=1&ipt=ef67becb8085ce75f051432ed02f25954cd31172ef19dd73a002db6b56d1cb8b&ipo=images height=350px>

:::
:::{.column width=50%}

<img src=https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmiro.medium.com%2Fmax%2F6000%2F1*3crbXAkyv6oXnqrm8QgB3g.jpeg&f=1&nofb=1&ipt=99e2e6cad4c3783174ce7907e2f265cfbb5ee8f7442e9b1c7072cebfc841e54f&ipo=images height=350px>

<img src=https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.vox-cdn.com%2Fthumbor%2F5xXTSpPnq7Cwz1_FztpjA-q0CRU%3D%2F0x0%3A1920x1280%2F1200x800%2Ffilters%3Afocal(807x487%3A1113x793)%2Fcdn.vox-cdn.com%2Fuploads%2Fchorus_image%2Fimage%2F69652248%2Fwindows11main.0.png&f=1&nofb=1&ipt=33b723d9e0210b9f5845ae7129c4b55e2866717dca2fef2bba4b4cca98dd3295&ipo=images height=350px>

:::
::::::

# De ontwerpcyclus

<img src=img/Ontwerpcyclus.png height=700px>

# De Opdracht

![](https://fgjbroeren.gitlab.io/ontwerpproject-bal-gooien/_images/Schets.png)

[https://bit.ly/po-ontwerpen](https://bit.ly/po-ontwerpen)

# Programma van Eisen

Een overzicht van wat het apparaat moet kunnen

## Randvoorwaarden

- Het apparaat is gemaakt van huis-tuin-en-keukenmaterialen
- Het apparaat past binnen de ontwerpruimte van 25 cm hoog en 40 cm in de werprichting

- De knikkers die geworpen worden hebben een diameter van 16,5 mm en wegen ongeveer 6 gram

## Functionele Eisen

- Het apparaat werpt een knikker over een obstakel en in een bakje
- Het apparaat komt in beweging na een eenvoudige handeling die geen energie toevoegt aan het systeem

## Prestatiecriteria

_Deze lijst geeft een begin, maar kan je nog aanvullen_

- Het apparaat is eenvoudig te activeren
- Het apparaat is eenvoudig terug te zetten zodat een nieuwe knikker gegooid kan worden
- De boog die de geworpen knikker maakt is telkens hetzelfde

# Conceptueel ontwerp

Wat moet het apparaat kunnen doen en hoe zouden we dat kunnen bereiken?

## Deelfuncties

Splits het grote probleem op in kleine stukjes

. . .

- Energie opslaan
- Mechanisme activeren/in beweging zetten
- Knikker werpen
- Knikker vasthouden

## Ideeëntabel

![](https://project.3me.tudelft.nl/2017/wb31/img/kaart/morfologischkaart.jpeg)

## Aan de slag!

- Lees de opdracht door
- Bekijk de deelfuncties, kan jij er nog andere bij verzinnen?
- Maak een ideeëntabel, probeer ten minste 3 ideeën per deelfunctie te bedenken

# Ontwerpvoorstel

Wat zijn de beste ideeën die we tot nu toe hebben bedacht en kunnen die gecombineerd worden? 

## Terug naar de ideeëntabel

![](https://project.3me.tudelft.nl/2017/wb31/img/kaart/morfologischkaart.jpeg)

## Aan de slag!

- Bekijk voor elke rij van je ideeëntabel wat het beste idee is
- Besluit of deze ideeën gecombineerd kunnen worden
  - Ja? Top!
  - Nee? Dan moet je toch een ander idee kiezen voor één van de rijen
- Maak een schets van het volledige ontwerp
- Beargumenteer waarom dit de beste ontwerprichting is

# Construeren

Past het echt zoals we gedacht hadden?

## Aan de slag!

- Snijden, lijmen enzovoorts op de tafels vooraan
- Vergeet niet foto's te maken van het proces
  - (juist) ook als het niet zo gaat als je verwacht
- Ga je onderdelen lijmen? Doe dat dan vandaag, dan kan de lijm drogen

# Testen

# Inleveren

- Filmpje van de test
- Verslag (zie website voor inhoud)

<br>

op ELO

uiterlijk __vrijdag 21 oktober, 23:59__