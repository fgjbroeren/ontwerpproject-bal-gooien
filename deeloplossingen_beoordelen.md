# Deeloplossingen Beoordelen en Ontwerpvoorstel Formuleren

In de tweede stap beoordelen we de deeloplossingen die in de ideeëntabel staan en kiezen we daaruit een goed werkende totaaloplossing. Deze totaaloplossing bestaat uit één oplossing per rij van je ideeëntabel. Daarmee zorg je er namelijk voor dat de totaaloplossing alle deelfuncties uit kan voeren.

## Deeloplossingen beoordelen

Voor elke rij van je ideeëntabel bekijk je welke deeloplossing het beste geschikt zou zijn. Hierbij houd je de prestatiecriteria uit het programma van eisen in gedachten. Zorg ervoor dat je uit kan leggen waarom je een keuze maakt, denk hier dus echt over na en kies niet zomaar één van de opties.

Als je voor elke deelfunctie een deeloplossing gekozen hebt kijk je nog of ze wel goed samengaan. Soms komt het voor dat deeloplossingen niet gecombineerd kunnen worden, bijvoorbeeld omdat ze elkaar in de weg zitten of van tegenstrijdige werkingsprincipes uitgaan. Dan zal je voor één van deze deelfuncties toch een andere, misschien minder ideale, oplossing moeten kiezen.

## Ontwerpvoorstel formuleren

Nu we een optimale combinatie van deeloplossingen hebben kunnen we het ontwerpvoorstel formuleren. Hiermee leggen wij aan de opdrachtgever uit welk ontwerp we gekozen hebben en waarom we verwachten dat dit het beste resultaat geeft.

Belangrijk hierbij is een schets van het totaalontwerp. Dit hoeft geen gedetailleerde tekening met alle onderdelen te zijn, maar moet wel geschikt zijn om de unieke eigenschappen van jullie ontwerp te communiceren. Laat dus goed zien hoe het ontwerp werkt en welke deeloplossingen bijdragen aan het functioneren. Het kan hierbij helpen om in een andere kleur pijlen toe te voegen om bewegingen te laten zien.

Naast de schets beschrijf je wat de belangrijkste eigenschappen van het ontwerp zijn en waarom je deze gekozen hebt.