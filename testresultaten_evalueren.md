# Testresultaten Evalueren

De laatste stap van de ontwerpcyclus is het evalueren van je testresultaten.

Tijdens het bouwen, testen, verbeteren en opnieuw testen heb je veel geleerd over hoe je ontwerp werkt. Aan het einde van de cyclus denk je hier nog eens goed over na en formuleer je verbeterpunten voor je ontwerp. Deze evaluatie kan het startpunt vormen voor een nieuwe iteratie in je ontwerp.

Bij de evaluatie van je ontwerp kan je de volgende vragen gebruiken:

- Voldoet het ontwerp aan alle functionele eisen?
- Deed het ontwerp tijdens de test wat ik verwacht had?
- Ben je het nog steeds eens met je keuze voor uitwerkingen uit je ideeëntabel? Zou je met je huidige kennis iets anders kiezen?
- Welke verandering zou het grootste (positieve) effect hebben op het functioneren van mijn ontwerp?