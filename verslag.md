# Het Eindverslag

Aan het einde van het project lever je een eindverslag in. In dit verslag leg je het ontwerpproces vast. Je legt uit welke keuzes je gemaakt heb en waarom, laat zien welke tests je uitgevoerd hebt en wat het resultaat daarvan was en laat natuurlijk het eindresultaat zien en legt uit wat er nog aan verbeterd zou kunnen worden. Het doel van dit verslag is __niet__ om jouw eindontwerp te verkopen, maar om te laten zien hoe je het ontworpen hebt en welke overwegingen daaraan ten grondslag liggen. Aan de hand van dit verslag zou een andere ontwerper verder moeten kunnen gaan met de ontwikkeling van de gooimachine zonder eerst nogmaals alle stappen die je al gedaan hebt uit te hoeven voeren.

Het eindverslag volgt de stappen uit de ontwerpcyclus en bestaat uit de volgende onderdelen. Deze onderdelen staan hieronder verder uitgelegd.

- [Een introductie](verslag-intro)
- [Programma van Eisen](verslag-pve)
- [Deeluitwerkingen](verslag-deeluitwerkingen)
- [Ontwerpvoorstel](verslag-ontwerpvoorstel)
- [Maakproces](verslag-realisatie)
- [Test en Evaluatie](verslag-testen)
- [Verbeterpunten](verslag-verbeteren)

(verslag-intro)=
## De Introductie

Je begint het verslag met een introductie van het probleem. Hier leg je uit welk ontwerpprobleem je hebt gekregen en waar dit relevant voor is. Hierbij zoek je nog wat extra achtergrondinformatie om aan de lezer duidelijk te maken wat je gaat doen en waarom.

(verslag-pve)=
## Programma van Eisen

In dit deel presenteer je het programma van eisen, dit is een overzicht van alle voorwaarden waaraan het ontwerp uiteindelijk moet voldoen. Dit is grotendeels al in de opdracht gegeven.

Denk eraan om alle deelfuncties ook duidelijk te presenteren. Deze heb je namelijk nodig bij het bedenken van de deeluitwerkingen.

(verslag-deeluitwerkingen)=
## Deeluitwerkingen

In dit deel presenteer je alle deeluitwerkingen die je bedacht hebt. Doe dit op een gestructureerde manier, bijvoorbeeld in een ideeëntabel. Licht elke deeluitwerking toe aan de hand van een kleine schets die het werkingsprincipe laat zien.

(verslag-ontwerpvoorstel)=
## Ontwerpvoorstel

In dit deel laat je zien welke combinatie van deeluitwerkingen je kiest en leg je uit waarom je denk dat deze combinatie tot de beste resultaten zal leiden. Centraal hierin is een schets van het complete ontwerp waarin alle deeluitwerkingen samen komen. 

(verslag-realisatie)=
## Maakproces

In dit deel laat je zien hoe je het ontwerp gebouwd hebt. Leg uit hoe het ging en welke problemen je tijdens het bouwen tegenkwam. Om dit te illustreren gebruik je de foto's die je tijdens het bouwen gemaakt hebt. 

(verslag-testen)=
## Test en Evaluatie

In dit deel geef je een verslag van de tests die je in de klas uitgevoerd hebt. Is het gelukt om de bal in het bakje te gooien?

Ook evalueer je het resultaat, is het gelukt om aan alle onderdelen van het programma van eisen te voldoen? Hoe goed scoor je op de prestatiecriteria?

(verslag-verbeteren)=
## Verbeterpunten

Tijdens het testen kom je altijd punten tegen die beter zouden kunnen aan je ontwerp. Daarom spreken we ook van een ontwerpcyclus: als je meer tijd zou hebben zou je op basis van de resultaten die je behaald hebt gaan werken aan een verbeterde versie. 

Helaas hebben wij die tijd niet in dit project. Daarom leg je in dit deel van het verslag uit wat er beter had gekunnen en geef je voor de twee belangrijkste verbeterpunten suggesties hoe dit er in de volgende versie uit zou kunnen zien.