# Activiteiten per les

In totaal zullen wij drie blokuren aan het ontwerpen besteden. Hier staat een grove planning van de ontwerpstappen die je in elke les uit moet voeren.

Tijdens al deze stappen kan je alvast aan het verslag werken. Denk er dus aan om tijdens het werk voldoende foto's te maken en om ook op te schrijven wat je doet en waarom.

| Lesuur | Activiteit |
| :--: | :--------------- |
| 1 | Uitleg, vragen stellen over de opdracht <br> [Conceptueel ontwerp: deelfuncties en deeloplossingen](conceptueel_ontwerp) |
| 2 | [Deeloplossingen beoordelen en ontwerpvoorstel formuleren](deeloplossingen_beoordelen) <br> [Start construeren](ontwerp_construeren) |
| 3 | [Ontwerp construeren](ontwerp_construeren) |
| 4 | [Ontwerp testen](ontwerp_testen) |
| 5 & 6 | [Testresultaten evalueren](testresultaten_evalueren) <br> [Verslag afmaken](verslag) |

