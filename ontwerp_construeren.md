# Ontwerp Construeren

Nu we een goed doordacht ontwerp hebben is het tijd om deze te gaan maken. In de klas zijn verschillende materialen aanwezig die je kan gebruiken om jouw ontwerp. Denk na over wat elk onderdeel moet doen en kies hier een geschikt materiaal bij. 

Tijdens het bouwen zal je vrijwel altijd merken dat het er in het echt toch anders uitziet dan in je eerdere tekeningen. Dingen passen niet helemaal goed in elkaar of werken toch niet zoals je gedacht had. Dat is allemaal onderdeel van een ontwerpproces. Bedenk nieuwe oplossingen, maak alternatieven en kom zo tot een werkend ontwerp. Leg deze aanpassingen en stappen ook vast met behulp van foto's, dat is namelijk wat we willen zien in het eindverslag.

Ten slotte is het ook een goed idee om je ontwerp af en toe al te testen. Kijk of het werkt zoals je verwacht had en of je de ontwerp al voldoet aan het programma van eisen. Als dat nog niet het geval is gaan we weer een stapje terug in de ontwerpcyclus. We analyseren eerst wat er nog mist, bedenken wat er zou moeten gebeuren, bedenken hoe dat zou kunnen en proberen het opnieuw. Zo zie je dat je binnen een ontwerpcyclus vaak nog veel kleine cycli uitvoert.