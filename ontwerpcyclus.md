(cyclus)=
# De Ontwerpcyclus

In dit project werk je volgens de ontwerpcyclus. Dit is een gestructureerde manier om een ontwerpproject aan te pakken. Door deze structuur te volgen is het makkelijker om gestructureerd te werken en je voortgang bij te houden. Daarnaast geeft het handvatten voor de verslaglegging en het overleg tussen uitvoerder en opdrachtgever. 

De ontwerpcyclus wordt ingedeeld in zes stappen: 

1. Analyseren en Beschrijven
2. Programma van Eisen opstellen
3. (deel)Uitwerkingen bedenken
4. Ontwerpvoorstel formuleren
5. Ontwerp Realiseren
6. Ontwerp Testen en Evalueren

![](https://media.natuurkunde.nl/content_files/files/4539/original/supportBinaryFiles_referenceId_0_supportId_42?1410264828)

Zoals je ook in de figuur ziet is dit een cyclus. Na het testen en evalueren zal je dus weer aan het begin beginnen door te kijken wat er goed ging, en wat er beter kon. Dat geeft weer het startpunt om het programma van eisen bij te stellen en nieuwe oplossingen te verzinnen.

Overigens voer je in de praktijk niet altijd al deze stappen uit. Soms, zoals ook bij dit project, geeft de oprachtgever al een programma van eisen. Dan kan je dus meteen beginnen met het bedenken van deeluitwerkingen. Daarnaast komt het wel eens voor dat je een paar stapjes terug moet. Dit kan bijvoorbeeld het geval zijn als je bij het realiseren van het ontwerp erachter komt dat toch niet alle onderdelen passen zoals je gedacht had. In dat geval moet je het ontwerpvoorstel bijwerken of zelfs nieuwe deeluitwerkingen bedenken om dit op te lossen.

# Meer informatie

Een uitgebreidere beschrijving van de Ontwerpcyclus is te vinden op [Natuurkunde.nl](https://www.natuurkunde.nl/artikelen/1869/technisch-ontwerpen-de-ontwerpcyclus)[^link]. Daar worden ook alle stappen afzonderlijk toegelicht.

[^link]: https://www.natuurkunde.nl/artikelen/1869/technisch-ontwerpen-de-ontwerpcyclus